import java.util.Scanner;

public class N2 {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String S = kb.next();
        System.out.println(ConvertStrToInt(S));
    }

    public static int ConvertStrToInt(String S) {
        int ans = 0;
        boolean negative = false;
        if (S.charAt(0) == '-') {
            negative = true;
        }
        for (int i = 0; i <= S.length() - 1; i++) {
            if (negative && i == 0)
                continue;
            ans = ans * 10 + S.charAt(i) - '0';
        }
        return negative ? -ans: ans;
    }

}
